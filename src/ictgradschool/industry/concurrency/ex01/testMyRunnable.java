package ictgradschool.industry.concurrency.ex01;

public class testMyRunnable {
    public static void main(String[] args) {
        myRunnable myRunnable = new myRunnable();
        Thread thread = new Thread(myRunnable);// create thread object
        thread.start();

        System.out.println("Interrupting thread and waiting for it to finish...");

        // Interrupting a thread requests that it terminates gracefully
        thread.interrupt();

        try {
            // Joining a thread waits for it to finish.
            thread.join();

        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }
}
