package ictgradschool.industry.concurrency.ex03;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.

        List<Thread> threads = new ArrayList<>();
        //Array list to stall all the pi runnable
        ArrayList<PIRunnable> runnables = new ArrayList<>();
        long numThread = 4;
        //we create 4 thread and each thread run pi runnable task
        for(int i = 0; i<numThread; i++){
            long numTask = numSamples/numThread;
            PIRunnable runnable = new PIRunnable(numTask);
            Thread t = new Thread(runnable);
            threads.add(t);
            runnables.add(runnable);
        }

        // Run all the threads
        for (Thread t : threads) {
            t.start();
        }

        // Wait for them all to finish
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long numInsideCircle = 0;

        for (PIRunnable runnable : runnables){
            numInsideCircle += runnable.getCounter();
        }

        double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
        return  estimatedPi;

    }


    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
